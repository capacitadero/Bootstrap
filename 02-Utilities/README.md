Utilities
==========

Utilidades básicas de Bootstrap.

Objetivo:
--------

-Conceptos básicos de Bootstrap.

-Identificar las utilidades básicas de Bootstrap para nuestro Proyecto Web.

Proceso:
-------

Asi como en el [Taller Introductorio]((https://gitlab.com/capacitadero/Bootstrap/tree/master/01-Intro) utilizamos las `Class` (Clases) para darle estilo a nuestras etiquetas. Bootstrap tabaja en base a clases ya definidas y que pueden añadirse a nuestro proyecto.

Por ejemplo, si tenemos una etiqueta `p` a la cual queremos darle determinada caracteristica según Bootstrap hariamos lo siguiente:
```html
 <p class="Aquí pondremos una o varias caracteristica">Mi texto</p>
 ```

Sin embargo, te preguntarás: Pero qué características puedo darle? Ahorita empezamos con ello.

## Colores:

Boostrap tiene colores predefinidos llamados Primary, Secondary, Success y muchas más. Para darle color a nuestro texto dentro de la etiqueta P debemos de darle las siguientes clases:
```html
<p class="text-primary">Mi texto</p>
<p class="text-secondary">Mi texto</p>
<p class="text-success">Mi texto</p>
<p class="text-danger">Mi texto</p>
<p class="text-warning">Mi texto</p>
<p class="text-info">Mi texto</p>
<p class="text-light">Mi texto</p>
<p class="text-dark">Mi texto</p>
<p class="text-muted">Mi texto</p>
<p class="text-white">Mi texto</p>
 ```

Vemos que algunas de nuestras letras no se pueden apreciar bien, entonces vamos a darle un `Background` con Bootstrap:
```html
<p class="text-light bg-dark">Mi texto</p>
<p class="text-white bg-dark">Mi texto</p>
 ```

Ya descubriste que para aplicar el `Background` simplemente debemos de añadir a nuestra clase un `bg-dark`

Tambien puedes utilizar otras características y ejemplos que nos brinda Bootstrap en su [Documentación Oficial](https://getbootstrap.com/docs/4.0/utilities/colors/)


## Alineación del texto:

En el Taller Introductorio aprendimos a utilizar propiedades como `Padding` o `Margin` para darle espacio a nuestros textos o elementos `HTML`, aquí podemos utilizar `text-left`, `text-center` y `text-right` para ubicar la posición de nuestro texto:
```html
<p class="text-left">Mi texto</p>
<p class="text-center">Mi texto</p>
<p class="text-right">Mi texto</p>
 ```
Tambien puedes utilizar otras clases que nos brinda Bootstrap como `text-justify` en su [Documentación Oficial](https://getbootstrap.com/docs/4.0/utilities/text/)

