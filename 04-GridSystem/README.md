Components
==========

Integrando Bootstrap a nuestro Proyecto por medio de los componentes.

Objetivo:
--------

-Conceptos básicos del Sistema Grid.

-Integrar el Sistema Grid de Bootstrap a nuestro Proyecto Web.

Proceso:
-------

Vamos a crear un nuevo archivo llamado `clase-grid.html` donde empezaremos a trabajar la sesión de hoy.

El Grid System(Sistema de Cuadrícula) de Bootstrap utiliza una serie de contenedores, filas y columnas para diseñar y alinear el contenido. 

Hoy vamos a empezar desde lo básico.

## class="container"

La clase Container es un elemento básico de diseño que se puede utilizar en Bootstrap.

Puedes tener un contenedor sensible de ancho fijo o ancho de fluido (lo que significa que es 100% ancho todo el tiempo).

Vamos a ver el primer ejemplo. Codifica lo siguiente dentro de tu archivo. (No te olvides mantener la estructura básica requerida para utilizar Bootstrap):
```html
<div class="container">
  <p>Aqui esta mi contenido dentro de un contenedor</p>
</div>
```

Ahora veremos como cambia si reemplazamos el código anterior por el siguiente:

```html
<div class="container-fluid">
  <p>Aqui esta mi contenido dentro de un contenedor fluido</p>
</div>
```

Puedes encontrar otros aspectos interesantes sobre el diseño y estructura en su [Documentación Oficial](https://getbootstrap.com/docs/4.0/layout/overview/)

## Grid System

El sistema Grid asigna clases a los diferentes `div` que utilizamos en nuestra web para su estructura.

Para empezar a utilizar el Sistema Grid hay que tener dos conceptos claro: ROW y COL (Filas y columnas).

La clase ROW nos ayuda a establecer filas dentro del diseño:

`class="row"`

La clase COL nos ayuda a establecer columnas dentro del diseño:

`class="col-12"`

Estas columnas sólo puede tener un valor máximo de 12 unidades. Y si tenemos varias columnas todas ellas debe de sumar tambien 12 unidades:

`class="col-4"` `class="col-4"` `class="col-4"`  = `12` Columnas

Àhora vamos a ver un ejemplo donde veremos que cada fila sólo puede tener un máximo de 12 columnas:
```html
<div class="container">
  <div class="row">
    <div class="col-4">
      Contenido de la primera columna
    </div>
    <div class="col-4">
      Contenido de la segunda columna
    </div>
    <div class="col-4">
      Contenido de la tercera columa
    </div>
  </div>
</div>
```

Y un ejemplo más sencillo con una columna seria:
```html
<div class="container">
  <div class="row">
    <div class="col-12">
      Una columna
    </div>    
  </div>
</div>
```

Lo interesante del Grid System es que podemos hacer que los componentes de nuestro sitio web se acomode según el tamaño de la pantalla que tiene el usuario(Celular, Laptop, Ipad, etc.)

Para poder realizar este efecto hay que darle determinados valor de tamaño a nuestro Sistema Grid.

Esos valores pueden ser: Extra small, Small, Medium, Large, Extra Large.

Los celulares podrían tener un valor de Extra small, las tables o laptops un valor de Small y otras laptops serían Medium. Todo esto es relativo ya que se puede utilizar a criterio del desarrollador y lo que el usuario piense.

Para el desarrollo de sitios web no necesitamos utilizar todos los tamaños, usualmente con dos es suficiente. Ya que por ejemplo, podemos utilizar Extra small para los equipos pequeños(celulares) y Small, este último se aplicará a tables, ipads, laptops y demás equipos.

Vamos a verlo mejor con un ejemplo:

`Queremos hacer un sitio web que sea adaptable a un celular y que apartir del tamaño Small cambie el diseño y estructura de todos sus elementos.`

```html
<div class="container">
  <div class="row">
    <div class="col-sm-4 col-12">
      Vista-Menú lateral en tamaño small y Menú superior cuando tamaño es menor al Small(es decir Extra Small)
    </div>
    <div class="col-sm-8 col-12">
      Vista-Contenido tamaño small y Contenido inferior cuando tamaño es menor al Small(es decir Extra Small)
    </div>
  </div>  
</div>
```

Analizaremos linea por linea:

Utilizamos la `class="container"` para tener un ancho fijo, podemos borrarlo o dejarlo `class=""` si no queremos tener este efecto.

`class="row"`  Establecemos una fila en esta ocasión.

Aquí viene lo interesante, como vemos el código, hay dos bloques(`div`) y cada bloque se acomodará según el valor de cada columna.

Si Bootstrap reconoce que el tamaño del navegador es de menor medida al Small utilizará `col-12` para cada bloque, es decir ocupará todo el ancho del navegador(es decir del tamaño de la pantalla del celular o equipo móvil).

Por otro lado, si el tamaño del navegador es superior al Small, la estructura de los dos bloques se organizará con dos columnas. Una de ellas sera de 4 unidades y otra de 8 unidades: `col-sm-4`  `col-sm-8`.
















