Bootstrap
==========

Bootstrap es un kit de herramientas de código abierto para desarrollar con HTML, CSS y JavaScript. Puede crear prototipos de forma rápida o aplicaciones completas.

Descripción:
-----------

Aquí aprenderás a utilizar de forma profesional Bootstrap contruyendo aplicaciones web a nivel Front-End. 

Requisitos Previos:
------------------

A nivel general, no existe requisito previo para aprender Boostrap. Sin embargo, es recomendable tener conocimiento previo de las etiquetas HTML, CSS y JavaScript.
Utilizaremos un Editor de Texto como Sublime Text, Atom o Bloc de Notas, según la preferencia del estudiante.
Pueden trabajar en cualquier sistema operativo.
Para estudiantes con discapacidad visual es necesario tener experiencia utilizando algún lector de pantalla para computadora: JAWS, NVDA, etc.

Tabla de contenido:
------------------


Contribución:
------------
Si quieres colaborar con Capacitadero es ideal comunicarte con nosotros porque queremos conocerte! Escribiendo a: capacitadero at gmail dot com

Licencia:
--------
Toda la documentación pertenece a Capacitadero sin embargo, buscamos aportar al mundo contenido constructivo libre de uso. Si utilizas nuestra documentación para fines positivos, estaremos felices.