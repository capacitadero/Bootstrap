Components
==========

Integrando Bootstrap a nuestro Proyecto por medio de los componentes.

Objetivo:
--------

-Conceptos básicos de Bootstrap.

-Integrar componentes de Bootstrap a nuestro Proyecto Web.

Proceso:
-------

Vamos a crear un nuevo archivo, por ejemplo: `componentes.html` donde empezaremos a utilizar componentes de Bootstrap.

No hay que olvidarnos de utilizar la plantilla o archivo básico que contiene los enlaces para utilizar Bootstrap.

Si no recuerdas, puedes volver a la carpeta [01-Intro](https://gitlab.com/capacitadero/Bootstrap/tree/master/01-Intro) donde esta nuestro archivo `hola.html`

## Botones

Como hemos visto en el Taller Introductorio, los botones son elementos importantes dentro de nuestro trabajo como desarrolladores. 

Podemos utilizarlo para enviar formularios, crear eventos específicos al hacerle click y mucho más.

En Bootstrap los botones están organizados de la siguiente manera:

```html
<button type="button" class="btn btn-primary">Primary</button>
```

El atributo `type` ya lo conocemos y en esta ocasión le damos el valor de `button`

Para darle el efecto de Bootstrap hay que asignarle una clase que contenga las características que queramos tenga nuestro Botón.

`Class="btn"` le asignamos un estilo de Botón(btn)

`Class="btn btn-primary"` a dicho Botón tendra un background o color de fondo llamado `Primary` en Bootstrap.

Más adelante aprenderemos a darle color personalizado a nustros botones.

A continuación, vemos nuestro Botón dentro del archivo `componentes.html` :
```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Componentes de Bootstrap</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
  </head>
  <body>

  		<button type="button" class="btn btn-primary">Primary</button>		
	
	<script src="js/jquery-3.3.1.slim.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>		
  </body>
</html>
```

Ahora, como sabemos, Bootstrap tiene multiples colores para utilizar, aqui vemos a todos ellos:
```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Componentes de Bootstrap</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
  </head>
  <body>
  		<button type="button" class="btn btn-primary">Primary</button>
		<button type="button" class="btn btn-secondary">Secondary</button>
		<button type="button" class="btn btn-success">Success</button>
		<button type="button" class="btn btn-danger">Danger</button>
		<button type="button" class="btn btn-warning">Warning</button>
		<button type="button" class="btn btn-info">Info</button>
		<button type="button" class="btn btn-light">Light</button>
		<button type="button" class="btn btn-dark">Dark</button>
		<button type="button" class="btn btn-link">Link</button>  
	
	<script src="js/jquery-3.3.1.slim.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>		
  </body>
</html>
```

### Grupo de Botones

A veces queremos tener botones agrupados. Por ejemplo, si realizamos una busqueda en Google, este agrupara nuestros resultados en números del `1 2 3`...`10`. Esto se llama Paginación. Aquí tenemos un código con el cual podriamos agrupar botones para una Paginación:

```html
<div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
  <div class="btn-group mr-2" role="group" aria-label="First group">
    <button type="button" class="btn btn-secondary">1</button>
    <button type="button" class="btn btn-secondary">2</button>
    <button type="button" class="btn btn-secondary">3</button>
    <button type="button" class="btn btn-secondary">4</button>
  </div>
  <div class="btn-group mr-2" role="group" aria-label="Second group">
    <button type="button" class="btn btn-secondary">5</button>
    <button type="button" class="btn btn-secondary">6</button>
    <button type="button" class="btn btn-secondary">7</button>
  </div>
  <div class="btn-group" role="group" aria-label="Third group">
    <button type="button" class="btn btn-secondary">8</button>
  </div>
</div>
```

Si analizamos el código observaremos que hay un `div` padre que contiene tres `div` hijos. 
El `div` padre tiene una clase `btn-tooblar` de Bootstrap, un atributo `role` que nos ayuda como desarrolladores 
a ingresasr información adicional de los elementos que contiene, aunque no es obligatorio añadirlo es muy importante para la accesibilidad. Luego esta `aria-label` se suele utilizar para darle un texto que describe los elementos que hay adentro. 
No es obligatorio utilizarlos, sin embargo es importante tenerlo en cuenta por temas de accesibilidad.

Los tres `div` hijos tiene a separan a los tres grupos de botones. Cada uno tiene su propia clase `btn-group` (boton-grupo). A los
dos primeros les dimos un `margin-right` de dos unidades(`mr-2`) con sus respectivos `role` y `aria-label`.

El tercero ya no tiene el `margin-right`. 

Dentro de los `div` hijos tenemos los botones con un color `secondary`.


Si quieres revisar más informaciónde los Botones puedes ingresar a la [Documentación Oficial](https://getbootstrap.com/docs/4.0/components/button-group/)

### Pagination

Bootstrap nos brinda una opción de Paginación utilizando las etiquetas que ya conocemos `ul`  y `li`

```html
<nav aria-label="Page navigation ejemplo">
  <ul class="pagination">
    <li class="page-item"><a class="page-link" href="#">Anterior</a></li>
    <li class="page-item"><a class="page-link" href="#">1</a></li>
    <li class="page-item"><a class="page-link" href="#">2</a></li>
    <li class="page-item"><a class="page-link" href="#">3</a></li>
    <li class="page-item"><a class="page-link" href="#">Siguiente</a></li>
  </ul>
</nav>
```

### Badges

Los Badges(insignias) son elementos `span` con una clase `badge` que al añadirle color queda muy bien:

```html
<h1>Códigos y Lenguajes de Programación:</h1>
<span class="badge badge-primary">HTML</span>
<span class="badge badge-secondary">CSS</span>
<span class="badge badge-success">Javascript</span>
<span class="badge badge-danger">Go</span>
<span class="badge badge-warning">Python</span>
<span class="badge badge-info">Objective-C</span>
<span class="badge badge-light">C++</span>
<span class="badge badge-dark">C</span>
```
Recuerda que `badge-primary` es lo necesario para darle el color `primary`.




