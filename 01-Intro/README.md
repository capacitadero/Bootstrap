Bootstrap
==========

Integrando Bootstrap a nuestro Proyecto.

Objetivo:
--------

-Conceptos básicos de Bootstrap.

-Integrar Bootstrap a nuestro Proyecto Web.

-Crear archivo hola.html

Proceso:
-------

Qué es Bootstrap?

Bootstrap es un Framework que nos ayuda a crear sitios web adaptables a equipos móviles y todo tipo
de superficies.

Inicialmente, Bootstrap fue desarrollado en Twitter por [Mark Otto](https://twitter.com/mdo) y [Jacob Thornton](https://twitter.com/fat) con el nombre de Twitter Blueprint. 

Mark comenta: 

`"Hecho por mí mismo y por Jacob Thornton, Bootstrap es un kit de herramientas de código abierto de front-end creado para ayudar a los diseñadores y desarrolladores a crear de manera rápida y eficiente cosas geniales en línea. Nuestro objetivo es proporcionar una biblioteca refinada, bien documentada y extensa de componentes de diseño flexibles construidos con HTML, CSS y JavaScript para que otros puedan construir e innovar."`

Adicionalmente, añade:

 `"(...)nos reunimos para diseñar y construir una nueva herramienta interna y vimos la oportunidad de hacer algo más. A través de ese proceso, nos vimos construyendo algo mucho más sustancial que otra herramienta interna. Meses después terminamos con una primera versión de Bootstrap como una manera de documentar y compartir bienes y patrones de diseño comunes dentro de la compañía.Después de un año de trabajar en él durante las noches y los fines de semana, abrimos Bootstrap a fines de agosto de 2010. Desde entonces se ha convertido en el proyecto más popular en GitHub(...)"`

## Integración:

Empezaremos a crear un nuevo archivo llamado `hola.html` con la estructura que ya hemos trabajado:

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Aprendiendo Bootstrap</title>
  </head>
  <body>
    <h1>Aprendiendo Bootstrap en Capacitadero!</h1>
  </body>
</html>
```

No hay que olvidar de añadir:

```html
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
```

Ya que nos garantiza una reproducción adecuada y un zoom táctil para todos los dispositivos.

---

En Bootstrap existen cuatro archivos importantes que son necesarios para su funcionamiento, tres son `Javascript` y uno `CSS`. Existen dos formas de acceder a ellos:

La Primera puede ser desde sus enlaces externos utilizando la etiqueta `<script>` para añadirlos a nuestro `hola.html`:

```html
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
```

y el archivo CSS por medio de `<link>`:

```html
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
```
Al final tendremos:

```html
<!DOCTYPE html>
<html lang="en">
  <head>    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">    
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <title>Aprendiendo Bootstrap</title>
  </head>
  <body>
    <h1>Aprendiendo Bootstrap en Capacitadero!</h1>  
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
</html>
```

Sin embargo, esta integración hace que nuestra web necesite mayor tiempo de carga ya que utiliza enlaces externos. Por ello, vamos a realizar la segunda forma: 

### Creando archivos propios con el código de los enlaces:

Dentro de nuestra carpeta de trabajo crearemos(en caso no lo tengas ya hecho) una carpeta `js` y `css` junto a nuestro archivo `hola.html`:

![Carpetas](img/carpetas.png)

Dentro de `js` guardaremos los archivos `.js` y en `css` los `.css`. 

Para ello, ingresaremos a los siguientes links ubicados dentro de los atributos `src` y `href` de las etiquetas `script` y `link` utilizadas anteriormente:

[https://code.jquery.com/jquery-3.3.1.slim.min.js](https://code.jquery.com/jquery-3.3.1.slim.min.js)

[https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js](https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js)

[https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js](https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js)

[https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css](https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css)

Vamos a realizar juntos el primer archivo Javascript. Ingresaremos al primer link, allí encontraremos un código el cual copiaremos y pegaremos en un nuevo archivo llamado `jquery-3.3.1.slim.min.js`. 

Tambien podriamos darle otro nombre como : `jquery-3.3.1.js`, `jquery.js` o el que queramos. 
Sin embargo, una recomendación es darle el nombre lógico y su versión para identificarlo más rápido.

Realizaremos lo mismo para los siguientes archivos Javascript:

`popper.min.js`, `bootstrap.min.js` y `bootstrap.min.css`. Este último dentro de la carpeta `css`.

Recordemos: Para que funcionen los componentes de Bootstrap se necesita el uso del lenguaje Javascript. Específicamente [Jquery](https://jquery.com/), [Poppers.js](https://popper.js.org/) y los propios de Bootstrap. Adicionalmente, la hoja de estilo(CSS) de Bootstrap.

Finalmente tendremos nuestro archivo `hola.html`

```html
<!DOCTYPE html>
<html lang="en">
  <head>    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">    
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <title>Aprendiendo Bootstrap</title>
  </head>
  <body>
    <h1>Aprendiendo Bootstrap en Capacitadero!</h1>  
    <script src="js/jquery-3.3.1.slim.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
```

 Recursos Adicionales:
--------------------

[Documentación Oficial](https://getbootstrap.com/docs/4.1/getting-started/introduction/) de Bootstrap.

Parte del Proceso fue extraido de [Bootstrap](https://v4-alpha.getbootstrap.com/about/history/) y el texto escrito por [Mark Otto](https://alistapart.com/article/building-twitter-bootstrap)






