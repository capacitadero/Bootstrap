
Components
==========

Integrando Bootstrap a nuestro Proyecto por medio de los contenidos.

Objetivo:
--------

-Conceptos básicos de Bootstrap.

-Integrar los contents de Bootstrap a nuestro Proyecto Web.

Proceso:
-------

## Tipografia

Como ya hemos aprendido en el Taller Introductorio, las etiquetas `h` son muy utilizadas, sobre todo para los títulos:

```html
<h1>h1 usando Bootstrap en Capacitadero</h1>
<h2>h2 usando Bootstrap en Capacitadero</h2>
<h3>h3 usando Bootstrap en Capacitadero</h3>
<h4>h4 usando Bootstrap en Capacitadero</h4>
<h5>h5 usando Bootstrap en Capacitadero</h5>
<h6>h6 usando Bootstrap en Capacitadero</h6>
```

Y tambien, con Bootstrap podriamos darle a la etiqueta `p` un estilo de la clase `h`:

```html
<p class="h1">h1 con etiquetas p usando Bootstrap en Capacitadero</p>
<p class="h2">h2 con etiquetas p usando Bootstrap en Capacitadero</p>
<p class="h3">h3 con etiquetas p usando Bootstrap en Capacitadero</p>
<p class="h4">h4 con etiquetas p usando Bootstrap en Capacitadero</p>
<p class="h5">h5 con etiquetas p usando Bootstrap en Capacitadero</p>
<p class="h6">h6 con etiquetas p usando Bootstrap en Capacitadero</p>
```

Si necesitemos tamaños más grandes, utilizando Bootstrap, podremos lograrlo por medio de clase `Display`

```html
<h1 class="display-1">Display 1</h1>
<h1 class="display-2">Display 2</h1>
<h1 class="display-3">Display 3</h1>
<h1 class="display-4">Display 4</h1>
```

## Inline text elements

## Naming a source

## Description list alignment

## Variables

## User input

## Images

## Tables

## Figures